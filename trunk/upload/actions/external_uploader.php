<?php
/*
 **************************************************************
 | Copyright (c) 2007-2010 Clip-Bucket.com. All rights reserved.
 | @ Author : ArslanHassan
 | @ Software : ClipBucket , � PHPBucket.com
 **************************************************************
*/
define("BACK_END",TRUE);
define("FRONT_END",FALSE);

/*
 Config.Inc.php
*/
require_once('../includes/common.php');
require_once('../includes/additional_definitions.php');


//Including Massuploader Class,
require_once('../includes/classes/mass_upload.class.php');
require_once('../includes/classes/user.class.php');
$cbmass 	= new mass_upload();


if (count($argv)!=2) {
	echo "incorrect argument count.\n Usage : ". $argv[0] . " <filekey>";
	exit(1);
}

$videofile=$argv[1];

if (! $cbmass->is_external_file($videofile)) {
	echo "uploaded video file or metadata file does not exists";
	exit(1);
}
$metafile = EXTERNAL_UPLOAD_DIR.'/'.substr($videofile, 0, strrpos($videofile, '.')) .".meta";
$meta = json_decode(file_get_contents($metafile), true);

if(!$meta) {
	echo "json decode error";
	exit(1);
}

$userquery->signup_user_if_doesnt_exist($meta['username']);

$file_key = time().RandomString(5);
$array = array
(
	'title' => $meta['title'],
	'description' => $meta['description'],
	'tags' => $meta['tags'],
	'category' => $meta['category'],
	'userid' => $userquery->get_userid_from_username($meta['username']),
	'file_name' => $file_key,
	'videokey' => $cbmass->video_keygen(),
   'broadcast' => $meta['broadcast'],
);

$vid = $Upload->submit_upload($array);

if(!$vid) {
        global $eh;
	echo "video submit failed : ".print_r($eh->error_list, true);
	exit(1);
}
//Moving file to temp dir and Inserting in conversion queue..
$file_name = $cbmass->move_external_to_temp($videofile, $file_key);
$Upload->add_conversion_queue($file_name);
echo $array['videokey'] . "\n";
exit(0);
?>