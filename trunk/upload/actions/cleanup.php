<?php
 /**
  * Written by : Stéphane Poinsart (on behalf of webtv.utc.fr)
  * Software : ClipBucket v2
  * License : Attribution Assurance License -- http://www.opensource.org/licenses/attribution.php
  * 
  * This file should run hourly, it make sure old connections get cleaned out of cb_sessions table
  **/

// suggested usage in /etc/crontab, every hour : 
/*

58 *    * * *   www-data   nice -n 17 php -q /usr/src/clipbucket/upload/actions/cleanup	.php > /dev/null

 */
	

ini_set('mysql.connect_timeout','6000');

$in_bg_cron = true;

include(dirname(__FILE__)."/../includes/config.inc.php");

// delete sessions unused for 3 days for logged users and 3 hours for unlogged users

$query="DELETE FROM ".tbl(sessions) ." WHERE last_active < (NOW() - INTERVAL 3 HOUR) AND session_string='guest'";
$db->Execute($query);

$query="DELETE FROM ".tbl(sessions) ." WHERE last_active < (NOW() - INTERVAL 72 HOUR) AND session_string<>'guest'";
$db->Execute($query);

?>