<?php
/*
 ***************************************************************
 | Copyright (c) 2007-2009 Clip-Bucket.com. All rights reserved.
 | @ Author : ArslanHassan
 | @ Software : ClipBucket , © PHPBucket.com
 ***************************************************************
*/
define("THIS_PAGE",'videos');
define("PARENT_PAGE",'videos');
require 'includes/config.inc.php';
$pages->page_redir();
$userquery->perm_check('view_videos',true);

//Setting Sort
$sort = $_GET['sort'];
$child_ids = "";

if(!$_GET['cat']) {
	$_GET['cat']=12;
}
if($_GET['cat'] && $_GET['cat']!='all')
{
	$childs = $cbvid->get_sub_categories(mysql_clean($_GET['cat']));
	$child_ids = array();
	if($childs)
		foreach($childs as $child)
		{
			$child_ids[] = $child['category_id'];
			$subchilds = $childs = $cbvid->get_sub_categories($child['category_id']);
			if($subchilds)
			foreach($subchilds as $subchild)
			{
				$child_ids[] = $subchild['category_id'];
			}
		}
	$child_ids[] = mysql_clean($_GET['cat']);
}

$vid_cond = array('category'=>$child_ids,'date_span'=>$_GET['time'],'sub_cats');

$params['featured']='yes';
//$params['category']=get_root_parent();
$params['order'] = ' datecreated DESC ';
$params = array_merge($params, $vid_cond);

assign('featured_vids',get_videos($params));

subtitle(lang('featured'));
//Displaying The Template
template_files('featured.html');
display_it();
?>