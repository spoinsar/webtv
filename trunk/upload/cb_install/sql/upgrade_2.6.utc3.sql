CREATE FULLTEXT INDEX stags ON cb_video (tags);
CREATE FULLTEXT INDEX sdescription ON cb_video (description);
CREATE FULLTEXT INDEX stitle ON cb_video (title);