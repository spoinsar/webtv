ALTER TABLE cb_video MODIFY broadcast char(16) NOT NULL;
ALTER TABLE cb_video ADD subtitles char(16) NOT NULL DEFAULT 'manual';