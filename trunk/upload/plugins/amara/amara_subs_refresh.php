<?php
/**
 * this file check if subtitles exists for a given video on amara.org,
 * and download them in all available languages.
 *
 * @Author : Stephane Poinsart
 * @Software : ClipBucket
 * @License : Attribution Assurance License -- http://www.opensource.org/licenses/attribution.php
 * @Since : Feb. 8 2013
 */

require_once(dirname(__FILE__)."/../../includes/config.inc.php");

$vfile=$argv[1];

if (! $vfile)
	exit(1);

$vfileurl="http://webtv.utc.fr/files/videos/$vfile-sd.mp4";

// DEBUG only : force fetching the video as if it were a file that exists on 
//$vfileurl="http://webtv.utc.fr/files/videos/13585009407d8a5-sd.mp4";

function amara_request($type, $urlpart_command, $args=array()) {	
	$amara_username=config('amara_username');
	$amara_apikey=config('amara_apikey');
	$amara_urlpart_base=config('amara_urlpart_base');
	$https_proxy=config('https_proxy');
	$urlpart_args="";
	
	if (!$amara_username || !$amara_apikey)
		exit(1);
	
	if (!$amara_urlpart_base)
		$amara_urlpart_base="https://amara.org/api/";

	switch ($type) {
		case "POST":
			// Not implemented & not needed
			curl_setopt($con, CURLOPT_POST, true);
			break;
		case "PUT":
			// Not implemented & not needed
			curl_setopt($con, CURLOPT_PUT, true);
			break;
		case "GET":
			foreach ($args as $arg => $value) {
				// first request arg is preceded by "?", next args are preceded by "&"
				if ($urlpart_args=="")
					$urlpart_args="?";
				else
					$urlpart_args.="&";
				$urlpart_args.=$arg."=".$value;
			}
			break;
	}
	$con=curl_init();
	curl_setopt($con, CURLOPT_TIMEOUT, 4);
	curl_setopt($con, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($con, CURLOPT_HTTPHEADER, array("X-api-username: $amara_username","X-apikey: $amara_apikey", "Cache-Control: no-cache", "Pragma: no-cache"));
	if ($https_proxy)
		curl_setopt($con, CURLOPT_PROXY, $https_proxy);
	curl_setopt($con, CURLOPT_URL, $amara_urlpart_base.$urlpart_command.$urlpart_args);
	$output=curl_exec($con);
	curl_close($con);
	return $output;
}

$amara_vidinfo=json_decode(amara_request("GET", "videos/", array("video_url"=>$vfileurl)),true);
if (!$amara_vidinfo || !$amara_vidinfo["objects"] || !$amara_vidinfo["objects"][0]) {
	exit(0);
	// probably video not registered on amara, maybe amara serverfail, anyway we can assume there is no subtitles we can use
}

$amara_vidinfo=$amara_vidinfo["objects"][0];

$amara_vidid=$amara_vidinfo["id"];

$languagelist=array();
foreach ($amara_vidinfo["languages"] as $language) {
	$languagelist[]=$language["code"];
}
foreach ($languagelist as $languagecode) {
	$amara_langinfo=json_decode(amara_request("GET", "videos/$amara_vidid/languages/$languagecode/"),true);
	if ($amara_langinfo['subtitles_complete']) {
		$subcontent=amara_request("GET", "videos/$amara_vidid/languages/$languagecode/subtitles/", array("format"=>"vtt"));
		if ($subcontent) {
			file_put_contents(BASEDIR."/files/subs/$vfile-$languagecode.vtt", $subcontent, LOCK_EX);
		}
	}
}

?>