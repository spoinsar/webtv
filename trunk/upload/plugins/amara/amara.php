<?php
/*
Plugin Name: Amara subtitles
Description: Automaticaly downloads amara.org subtitles if they exist for each watched video
Author: Stephane Poinsart, on behalf of www.utc.fr
Author Website: http://webtv.utc.fr
ClipBucket Version: 2
Version: 1.0
Plugin Type: global
*/

/**
 * triggered when a video is watched, check if last rub_refresh is old, run it if it is
 *
 * @Author : Stephane Poinsart
 * @Software : ClipBucket
 * @License : Attribution Assurance License -- http://www.opensource.org/licenses/attribution.php
 * @Since : Feb. 8 2013
 */

// will request subtitles every AMARA_CACHE_TIMEOUT
define("AMARA_CACHE_TIMEOUT", 60);

// allready included and causing conflicts with admin-config
//require_once(dirname(__FILE__)."/../../includes/config.inc.php");

register_actions_play_video("amara_refresh");


function amara_refresh($data) {
	$vfile = $data['vdetails']['file_name'];
	if ($data['vdetails']['subtitles'] && $data['vdetails']['subtitles']!='' && $data['vdetails']['subtitles']!="amara")
		return;
	
	// if APC cache is installed (apt-get install php-apc)
	// subtitles will be refreshed every AMARA_CACHE_TIMEOUT interval
	// if not, we attempt to re-download them everytime
	if (function_exists("apc_fetch") && function_exists("apc_store")) {
		$amara_cache=apc_fetch("amara_cache_$vfile");
//echo "checking the cache...";
		
		// no previous
		if (!$amara_cache) {
			apc_store("amara_cache_$vfile", true, AMARA_CACHE_TIMEOUT);
		} else {
			// subtitles are in APC cache = we return now to avoid the download command
//echo "subtitles already cached";
			return;
		}
	}
//echo "running command : nohup php ".BASEDIR."/plugins/amara/amara_subs_refresh.php $vfile 2>/dev/null 1>/dev/null &";
	// nohup seems necessary to avoid php beeing closed at the end of the current connection
	exec("nohup php ".BASEDIR."/plugins/amara/amara_subs_refresh.php $vfile 2>/dev/null 1>/dev/null &");
}

?>