<?php
/*
 ****************************************************************
 | Copyright (c) 2007-2010 Clip-Bucket.com. All rights reserved.
 | @ Author   : ArslanHassan
 | @ Software : ClipBucket , © PHPBucket.com
 *****************************************************************
*/
define('THIS_PAGE','search_result');
require 'includes/config.inc.php';
$pages->page_redir();

$page = mysql_clean($_GET['page']);
$type = mysql_clean($_GET['type']) ;
$type = $type ? $type : 'videos';
$chkType = $type;
//Checking if search for specific section is allowed or not
if($type=='users')	$chkType = 'channels';
isSectionEnabled($chkType,true);

$search = cbsearch::init_search($type);

$search->key = mysql_clean($_GET['query']);
$key = mysql_clean($_GET['query']);

# TODO : comme l'envoi de tableau par GET n'est pas standard, il faudrait trouver
# un endroit dans clipbucket où ils le font pour etre cohérent dans les dev futures
if(!is_array($_GET['category']))
	$_GET['category'] = array(mysql_clean($_GET['category']));

	
// recherche pondérée, en fonction de
// la correspondance des mots clés, surtout pour le titre / tags et un peu pour le contenu
// les vidéos récement ajoutées (gros bonus pour quelques jours, petit pour quelques semaines, infime pour quelques mois)
// le nombre de vues de la vidéo
$q="
SELECT
	".tbl("video").".*,
	".tbl("users").".username,
	MATCH(title) AGAINST ('".$key."') + MATCH(tags) AGAINST ('".$key."') + MATCH(description) AGAINST ('".$key."')*0.5 AS searchscore,
	LOG10(GREATEST(views,10))-1 AS popscore,
	80/(7+DATEDIFF(NOW(),date_added)) as datescore,
	(".tbl("video").".featured='yes')*3 as featuredscore
FROM ".tbl("video")."
LEFT JOIN ".tbl("users")." ON ".tbl("video").".userid=".tbl("users").".userid
WHERE
		broadcast='public'
		AND active='yes'
HAVING searchscore>0.1
ORDER BY (searchscore+popscore+datescore+featuredscore) DESC
LIMIT 25 OFFSET ".(25*($page-1))."
";

$qcount="SELECT COUNT(*) AS c
FROM ".tbl("video")."
WHERE
		broadcast='public'
		AND active='yes'
		AND MATCH(title) AGAINST ('".$key."') + MATCH(tags) AGAINST ('".$key."') + MATCH(description) AGAINST ('".$key."')*0.5>0.1
";
//echo $q;
$results=array();
$query = mysql_query($q);
while ($r = mysql_fetch_array($query, MYSQL_ASSOC))
{
	$results[$r['videoid']]=$r;
}
$rcount = mysql_query($qcount);
$row = mysql_fetch_array($rcount, MYSQL_ASSOC);
$total_rows= $row['c'];

$total_pages = count_pages($total_rows,25);

//Pagination
$pages->paginate($total_pages,$page);

// On va associer à chaque vidéo le nom de ses catégories et des catégories parentes éventuelles
$categories = getCategoryList(array("type"=>'video', "use_sub_cats"=>false));
foreach($categories as $id=>$cat){
   $categories[$id]['parents'] = array();
   $parent = $cat['parent_id'];
   while($parent != 0){
      $categories[$id]['parents'][] = $parent;
      $parent = $categories[$parent]['parent_id'];
   }
}

foreach($results as $id=>$v){
   $v_cat_ids = str_replace('#', '', explode(' ', $v['category']));
   $results[$id]['categories'] = array();
   foreach($v_cat_ids as $cat_id){
      if(is_numeric($cat_id)){
         $results[$id]['categories'][] = $categories[$cat_id]['category_name'];
         if (isset($categories[$cat_id]) && isset($categories[$cat_id]['parents'])) {
         	foreach($categories[$cat_id]['parents'] as $p){
            	$results[$id]['categories'][] = $categories[$p]['category_name'];
         	}
         }
      }
   }
   $results[$id]['categories'] = join(' ', array_unique($results[$id]['categories']));
}


Assign('results',$results );
Assign('template_var',$search->template_var);
Assign('display_template',$search->display_template);
if(empty($search->key))
	Assign('search_type_title',$search->search_type[$type]['title']);
else
	Assign('search_type_title',sprintf(lang('searching_keyword_in_obj'),mysql_clean(get('query')),$search->search_type[$type]['title']));

if(mysql_clean(get('query')))
{
	subtitle($search->search_type[$type]['title'].' &raquo; '.mysql_clean(get('query')));
}



//Displaying The Template
template_files('search.html');
display_it();
//pr($db);
?>