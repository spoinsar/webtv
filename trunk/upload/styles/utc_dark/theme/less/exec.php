<?php
/*==============================================================================
 * Usage :
 *    Ce fichier ne doit pas être appelé directement.
 * Les fichiers LESS doivent être appelés directement par le client, et un HTACCESS
 * doit nous rediriger "silentieusement" ici, de cette manière le REQUEST_URI
 * correspond au fichier LESS voulu.
 *
 * ex de .htaccess :
 *    RewriteEngine On
 *    RewriteRule ^(.*)\.less$ less/exec.php [L]
 *
 * Cette méthode est celle que j'ai trouvée la plus simple pour utiliser LESS :
 *   - fichiers LESS intacts (pas PHP en plus comme je l'avais pensé au début)
 *   - liens vers les imports et images préservés
 *   - URLs simples : identiques aux appels CSS
 *   - fonctionne avec des sous-dossiers
 *
 * Structure actuelle :
 * Dossier CSS
 *    |
 *    +-- fichier CSS
 *    +-- fichier CSS
 *    +-- fichier LESS
 *    +-- Dossier librairie
 *    |     +-- exec.php
 *    |     +-- lessc.inc.php
 *   [+-- Dossier
 *    |     +-- fichier LESS
 *    |     +-- fichier LESS]
 *    ...
 *
 *=============================================================================*/



#==============================================================================
# Config
#==============================================================================
$cache_dir = "/tmp"; # must exist and be writable by apache user
$root_dir = $_SERVER['DOCUMENT_ROOT']; # Root of the Web server
$lessphp = 'lessphp/lessc.inc.php';


#==============================================================================
# Main
#==============================================================================
$css_filepath = auto_compile_less();

header('Content-type: text/css');
@readfile($css_filepath);
exit;


/**
 * Autocompile le fichier LESS et renvoie le chemin vers le CSS résultant.
 *
 * A partir de REQUEST_URI, détermine automatiquement quel fichier doit être
 * compilé. Si une version compilée de ce fichier existe déjà, il regardera
 * si l'un des fichiers a changé (actuel fichier LESS + imports) pour savoir
 * s'il doit le recompiler ou non.
 */
function auto_compile_less() {

  #config
  global $cache_dir, $root_dir, $lessphp;

  # Extrait les infos du chemin vers le fichier LESS
  $less_filepath = "$root_dir{$_SERVER['REQUEST_URI']}";
  $pi = pathinfo($less_filepath);

  # Construit les infos du fichier CSS résultat.
  # Note : le md5 n'est là que pour différencier les fichiers de mêmes noms
  # venant de répertoires différents
  $css_filename = md5($pi['dirname']) ."-{$pi['filename']}.css";
  $css_filepath = "$cache_dir/$css_filename";

  # Charge le cache
  $cache_filepath = $css_filepath.".cache";
  if (file_exists($cache_filepath)) {
    $cache = unserialize(file_get_contents($cache_filepath));
  } else {
    $cache = $less_filepath;
  }

  # Execute
  require($lessphp);
  $new_cache = lessc::cexecute($cache);
  if (!is_array($cache) || $new_cache['updated'] > $cache['updated']) {
    file_put_contents($cache_filepath, serialize($new_cache));
    file_put_contents($css_filepath, $new_cache['compiled']);
  }

  return $css_filepath;
}

