<?php
/*
 ****************************************************************
 | Copyright (c) 2007-2010 Clip-Bucket.com. All rights reserved.
 | @ Author : ArslanHassan
 | @ Software : ClipBucket , © PHPBucket.com
 ****************************************************************
*/

define("THIS_PAGE",'user_videos');
define("PARENT_PAGE",'videos');

require 'includes/config.inc.php';
$pages->page_redir();
$userquery->perm_check('view_videos',true);

$u = $_GET['user'];
$u = $u ? $u : $_GET['userid'];
$u = $u ? $u : $_GET['username'];
$u = $u ? $u : $_GET['uid'];
$u = $u ? $u : $_GET['u'];
$u = $u ? $u : $userquery->username;

$udetails = $userquery->get_user_details($u);
$page = mysql_clean($_GET['page']);

$sort = $_GET['sort'];
switch($sort)
{
	case 'most_recent':
	default: 
        $order = ' date_added DESC '; 
        break;
    case 'oldest': 
        $order = ' date_added ASC '; 
        break;
}

if($udetails)
{

   if(!$userquery->perm_check('admin_access') && $udetails['userid'] != userid()){
      e();
      $Cbucket->show_page = false;
   }else{

	assign("u", $udetails);
      $mode = $_GET['mode'];

      assign("u", $udetails);
      assign('p', $userquery->get_user_profile($udetails['userid']));

      switch ($mode) {
          
         case 'uploads':
         case 'videos':
         default: {
               
                assign("the_title", $udetails['username'] . " videos");
               subtitle(sprintf(lang("users_videos"), $udetails['username']));
               
               # get videos
               $params = array(
                   'user' => $udetails['userid'], 
                   'limit' => create_query_limit($page, config('videos_items_uvid_page')), 
                   'show_hidden' => true, 
                   'order'=>$order
               );
               $videos = get_videos($params);
               
               # count totals
               unset($params['limit']);
               $params['count_only'] = true;
               $total_rows = get_videos($params);
               $total_pages = count_pages($total_rows, config('videos_items_uvid_page'));
            }
            break;
        
         case 'favorites': {
             
               assign("the_title", $udetails['username'] . " favorites");
               subtitle(sprintf(lang("title_usr_fav_vids"), $udetails['username']));
               
               # get videos
               $params = array(
                   'user' => $udetails['userid'], 
                   'limit' => create_query_limit($page, config('videos_items_ufav_page')), 
                   'order'=>$order
               );
               $videos = $cbvid->action->get_favorites($params);
               
               # count totals
               unset($params['limit']);
               $params['count_only'] = true;
               $total_rows = $cbvid->action->get_favorites($params);
               $total_pages = count_pages($total_rows, config('videos_items_ufav_page'));
            }
      }

      Assign('videos', $videos);


//Pagination
      $pages->paginate($total_pages, $page);
   }
}else{
	e(lang("usr_exist_err"));
	$Cbucket->show_page = false;
}


template_files('user_videos.html');
display_it();
?>