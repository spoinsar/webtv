<?php

/**
 * This is new file for conversion
 * now , FFMPEG will only be used for video conversion
 * @Author : Arslan Hassan
 * @Software : ClipBucket
 * @License : Attribution Assurance License -- http://www.opensource.org/licenses/attribution.php - You Cannot MODIFY - REUSE THIS FILE
 * @website : http://clip-bucket.com/
 */


define("KEEP_MP4_AS_IS",config('keep_mp4_as_is'));
define('FFMPEG_BINARY', get_binaries('ffmpeg'));
define('FFPROBE_BINARY', get_binaries('ffprobe'));
define('MPLAYER_BINARY', get_binaries('mplayer'));
define('PROCESSESS_AT_ONCE',config('max_conversion'));

class ffmpeg 
{
	var $input_details = array(); //Holds File value
	var $output_details = array(); // Holds Converted File Details
	var $ffmpeg; // path to ffmpeg binary
	var $ffprobe;
	var $input_file; //File to be converted
	var $output_file; //File after $file is converted
	var $tbl = 'video_files';
	var $row_id  ; //Db row id
	var $log; //Holds overall status of conversion
	var $start_time;
	var $end_time;
	var $total_time;
	var $configs = array();
	var $gen_thumbs; //If set to TRUE , it will generate thumbnails also
	var $remove_input = TRUE;
	var $gen_big_thumb = FALSE;
	var $h264_single_pass = FALSE;
	var $hq_output_file = '';
	var $log_file = '';
	var $input_ext = '';
	var $tmp_dir = '/';
	var $mplayer = '';
	var $thumb_dim = '120x90'; //Thumbs Dimension
	var $num_of_thumbs = '3'; //Number of thumbs
	var $big_thumb_dim = 'original'; //Big thumb size , original will get orginal video window size thumb othersie the dimension
	var $failed_reason = '';
	
	
	var $vconfigs = array(); //Version Configs
	
	/**
	 * Initiating Class
	 */
	function ffmpeg($file)
	{
		global $Cbucket;
		$this->ffmpeg = FFMPEG_BINARY;
		$this->ffprobe = FFPROBE_BINARY;
		$this->mplayer = MPLAYER_BINARY;
		$this->input_file = $file;
	}
	
	
	/**
	 * Prepare file to be converted
	 * this will first get info of the file
	 * and enter its info into database
	 */
	function prepare($file=NULL)
	{
		global $db;
		
		if($file)
			$this->input_file = $file;
			
		if(file_exists($this->input_file))
			$this->input_file = $this->input_file;
		else
			$this->input_file = TEMP_DIR.'/'.$this->input_file;
		
		//Checking File Exists
		if(!file_exists($this->input_file))
		{
			$this->log('File Exists','No');
		}else{
			$this->log('File Exists','Yes');
		}
		
		//Get File info
		$this->input_details = $this->get_file_info(NULL, true);
		
		//Loging File Details
		$this->log .= "\nPreparing file...\n";
		$this->log_file_info();
		
		//Insert Info into database
		//$this->insert_data();		
		
		//Get FFMPEG version
		$result = shell_output(FFMPEG_BINARY." -version");
		$version = parse_version('ffmpeg',$result);
		
		
		$this->vconfigs['map_meta_data'] = 'map_meta_data';
		
		if(strstr($version,'Git'))
		{
			$this->vconfigs['map_meta_data'] = 'map_metadata';
		}
	}
	
	
	
	/**
	 * Function used to get file information using FFMPEG
	 * @param FILE_PATH
	 */
	 function get_file_info($path_source=NULL, $volumedetect=false) {
		
		if(!$path_source)
			$path_source = $this->input_file;
			
		# init the info to N/A
		$info['format']			= 'N/A';
		$info['duration']		= 'N/A';
		$info['size']			= 'N/A';
		$info['bitrate']		= 'N/A';
		$info['video_width']	= 'N/A';
		$info['video_height']	= 'N/A';
		$info['video_wh_ratio']	= 'N/A';
		$info['video_codec']	= 'N/A';
		$info['video_rate']		= 'N/A';
		$info['video_bitrate']	= 'N/A';
		$info['video_color']	= 'N/A';
		$info['audio_codec']	= 'N/A';
		$info['audio_bitrate']	= 'N/A';
		$info['audio_rate']		= 'N/A';
		$info['audio_channels']	= 'N/A';
		$info['volgain']		= 'N/A';
		$info['volcompress']	= 'N/A';
		$info['path']			= $path_source;
		
		# get the file size
		$stats = @stat( $path_source );
		if( $stats === false )
			$this->log .= "Failed to stat file $path_source!\n";
		$info['size'] = (integer)$stats['size'];
		
		if ($volumedetect) {
			$this->raw_output = $output = $this->exec( $this->ffmpeg." -i $path_source -af volumedetect -f null /dev/null 2>&1" );
	 	} else {
	 		$this->raw_output = $output = $this->exec( $this->ffmpeg." -i $path_source -codec copy -f null /dev/null 2>&1" );
	 	}
		
		$this->raw_info = $info;
		# parse output
		if( $this->parse_format_info( $output ) === false )
			return false;
		$info = $this->raw_info;
		return $info;
	}
	
	
	
	/**
	 * Function used to excute SHELL Scripts
	 */
	function exec( $cmd ) {
		// flush logs because programs might redirect stdout to them
		$this->write_logs();
		return shell_exec($cmd);
	}
	
	function pregMatch($in,$str)
	{	
		preg_match("/$in/",$str,$args);
		return $args;
	}
	
	/**
	 * Author : Arslan Hassan
	 * parse format info
	 * 
	 * output (string)
	 *  - the ffmpeg output to be parsed to extract format info
	 * 
	 * info (array)
	 *  - see function get_encoding_progress
	 * 
	 * returns:
	 *  - (bool) false on error
	 *  - (bool) true on success
	 */
	 
	function parse_format_info( $output ) {
		$this->raw_info;
		$info =  $this->raw_info;
		# search the output for specific patterns and extract info
		# check final encoding message
		if($args =  $this->pregMatch( 'Unknown format', $output) ) {
			$Unkown = "Unkown";
		} else {
			$Unkown = "";
		}
		if( $args = $this->pregMatch( 'video:([0-9]+)kB audio:([0-9]+)kB (subtitle:[^ ]+ )?(other streams:[^ ]+ )?global headers:[0-9]+kB muxing overhead', $output) ) {
			$video_size = (float)$args[1];
			$audio_size = (float)$args[2];
		} else {
			$this->log.="Failed to parse a/v bitrate info";
			return false;
		}

		// check for last enconding update message
		if($args = $this->pregMatch( '(frame=([^=]*) fps=[^=]* q=[^=]* L)?size=([^=]*kB|N\/A) time=([^=]*) bitrate=([^=]*kbits\/s|N\/A)[^=]*$', $output) ) {
			$frame_count = $args[2] ? (float)ltrim($args[2]) : 0;
			$duration    = (float)$args[3];
		} else {
			$this->log.="Failed to parse fps or time info info";
			return false;
		}
		// what should we add as raw volume gain (check for absolute max volume)
		if($args = $this->pregMatch( 'Parsed_volumedetect.* max_volume: -?(.*) dB', $output) ) {
			$volgain = $args[1] ? (int)ltrim($args[1])-1 : 0;
			if ($volgain<0)
				$volgain=0;
			if ($volgain>10)
				$volgain=10;
			$info['volgain'] = $volgain;
			
			// what could we add as volume compression (normalize based on the 0.03% highest volume samples)
			// we use dynamic volume normalization instead, but it's still used to determine if the filter is needed
			$volcompress= 0;
			if($args = $this->pregMatch('Parsed_volumedetect.* n_samples: (.*)', $output) ) {
				$n_samples=(float)($args[1] ? ltrim($args[1]) : 0);
				$low_samples=1.0;
				for ($i=1; $i<15;$i++) {
					if($args = $this->pregMatch('Parsed_volumedetect.* histogram_'.$i.'db: (.*)', $output) ) {
						$low_samples+=(float)($args[1] ? ltrim($args[1]) : 0);
						if ($n_samples/$low_samples<3000.0) {
							$volcompress=$i;
							break;
						}
					}
				}
				if ($volcompress==0 && $low_samples>100) {
					$volcompress=15;
				}
				if ($volcompress<$volgain)
					$volcompress=$volgain;
				$info['volcompress']=$volcompress;
			}
		}
		
		if(!$duration)
		{
			$duration = $this->pregMatch( 'Duration: ([0-9.:]+),', $output );
			$duration    = $duration[1];
			
			$duration = explode(':',$duration);
			//Convert Duration to seconds
			$hours = $duration[0];
			$minutes = $duration[1];
			$seconds = $duration[2];
			
			$hours = $hours * 60 * 60;
			$minutes = $minutes * 60;
			
			$duration = $hours+$minutes+$seconds;
		}

		$info['duration'] = $duration;
		if($duration)
		{
			$info['bitrate' ] = (integer)($info['size'] * 8 / 1024 / $duration);
			if( $frame_count > 0 )
				$info['video_rate']	= (float)$frame_count / (float)$duration;
			if( $video_size > 0 )
				$info['video_bitrate']	= (integer)($video_size * 8 / $duration);
			if( $audio_size > 0 )
				$info['audio_bitrate']	= (integer)($audio_size * 8 / $duration);
				# get format information
			if($args =  $this->pregMatch( "Input #0, ([^ ]+), from", $output) ) {
				$info['format'] = $args[1];
			}
		}

		# get video information
		$args= $this->pregMatch( '([0-9]{2,4})x([0-9]{2,4})', $output );
		if (count($args)==3) {			
			$info['video_width'  ] = $args[1];
			$info['video_height' ] = $args[2];
			$args= $this->pregMatch( ' DAR ([0-9]{1,4}):([0-9]{1,4})', $output );
			if (count($args)==3 && $args[1]>1.0 && $args[2]>1.0 && ($args[1]/$args[2])>0.9 && ($args[1]/$args[2])<2.0) {
				// if all is well, use true aspect ratio
				$info['video_wh_ratio'] = (float)$args[1] / (float)$args[2];
			} else {
				// if DAR match failed, or if final aspect ratio is unconventionnal, assume square pixels and use video pixel count
				$info['video_wh_ratio'] = (float)$info['video_width'] / (float)$info['video_height'];
			}
		}

		
		if($args= $this->pregMatch('Video: ([^ ^,]+)',$output))
		{
			$info['video_codec'] = $args[1];
		}

		# get audio information
		if($args =  $this->pregMatch( "Audio: ([^ ]+), ([0-9]+) Hz, ([^\n,]*)", $output) ) {
			$audio_codec = $info['audio_codec'   ] = $args[1];
			$audio_rate = $info['audio_rate'    ] = $args[2];
			$info['audio_channels'] = $args[3];
		}
		
		if(!$audio_codec || !$audio_rate)
		{
			$args =  $this->pregMatch( "Audio: ([a-zA-Z0-9]+)(.*), ([0-9]+) Hz, ([^\n,]*)", $output);
			$info['audio_codec'   ] = $args[1];
			$info['audio_rate'    ] = $args[3];
			$info['audio_channels'] = $args[4];
		}
		
		
		$this->raw_info = $info;
		# check if file contains a video stream
		return $video_size > 0;

		#TODO allow files with no video (only audio)?
		#return true;
	}
	
	/**
	 * Function used to insert data into database
	 * @param ARRAY
	 */
	
	function insert_data()
	{
		global $db;
		//Insert Info into database
		if(is_array($this->input_details))
		{
			foreach($this->input_details as $field=>$value)
			{
				$fields[] = 'src_'.$field;
				$values[] =  $value;
			}
			$fields[] = 'src_ext';
			$values[] = getExt($this->input_details['path']);
			$fields[] = 'src_name';
			$values[] = getName($this->input_details['path']);
			
			$db->insert(tbl($this->tbl),$fields,$values);	
			$this->row_id = $db->insert_id();
		}
	}
	
	/**
	 * Function used to update data of
	 */
	function update_data($conv_only=false)
	{
		global $db;
		//Insert Info into database
		if(is_array($this->output_details) && !$conv_only)
		{
			foreach($this->output_details as $field=>$value)
			{
				$fields[] = 'output_'.$field;
				$values[] = $value;
			}		
			$fields[] = 'file_conversion_log';
			$values[] = $this->log;
			$db->update(tbl($this->tbl),$fields,$values," id = '".$this->row_id."'");	
		}else
			$fields[] = 'file_conversion_log';
			$values[] = $this->log;
			$db->update(tbl($this->tbl),$fields,$values," id = '".$this->row_id."'");	
	}
	
	
	/**
	 * Function used to add log in log var
	 */
	function log($name,$value)
	{
		$this->log .= $name.' : '.$value."\r\n";
	}
	
	/**
	 * Function used to start log
	 */
	function start_log()
	{
		$this->log = "Started on ".NOW()." - ".date("Y M d")."\r\n\n";
		$this->log .= "Checking File ....\r\n";
		$this->log('File',$this->input_file);
	}
	
	/**
	 * Function used to log video info
	 */
	function log_file_info()
	{
		$details = $this->input_details;
		if(is_array($details))
		{
			foreach($details as $name => $value)
			{
				$this->log($name,$value);
			}
		}else{
			$this->log .=" Unknown file details - Unable to get video details using FFMPEG \n";
		}
	}
	/**
	 * Function log outpuit file details
	 */
	function log_ouput_file_info()
	{
		$details = $this->output_details;
		if(is_array($details))
		{
			foreach($details as $name => $value)
			{
				$this->log('output_'.$name,$value);
			}
		}else{
			$this->log .=" Unknown file details - Unable to get output video details using FFMPEG \n";
		}
	}
	 
	
	
	
	/**
	 * Function used to time check
	 */
	function time_check()
	{
		$time = microtime();
		$time = explode(' ',$time);
		$time = $time[1]+$time[0];
		return $time;
	}
	
	/**
	 * Function used to start timing
	 */
	function start_time_check()
	{
		$this->start_time = $this->time_check();
	}
	
	/**
	 * Function used to end timing
	 */
	function end_time_check()
	{
		$this->end_time = $this->time_check();
	}
	
	/** 
	 * Function used to check total time 
	 */
	function total_time()
	{
		$this->total_time = round(($this->end_time-$this->start_time),4);
	}
	
	
	/**
	 * Function used to calculate video padding
	 */
	function calculate_size_padding( $parameters, $source_info, & $width, & $height, & $ratio, & $pad_top, & $pad_bottom, & $pad_left, & $pad_right )	
	{
		$p = $parameters;
		$i = $source_info;

		switch( $p['resize'] ) {
			# dont resize, use same size as source, and aspect ratio
			# WARNING: some codec will NOT preserve the aspect ratio
			case 'no':
				$width      = $i['video_width'   ];
				$height     = $i['video_height'  ];
				$ratio      = $i['video_wh_ratio'];
				$pad_top    = 0;
				$pad_bottom = 0;
				$pad_left   = 0;
				$pad_right  = 0;
				break;
			# resize to parameters width X height, use same aspect ratio
			# WARNING: some codec will NOT preserve the aspect ratio
			case 'WxH':
				$width  = $p['video_width'   ];
				$height = $p['video_height'  ];
				$ratio  = $i['video_wh_ratio'];
				$pad_top    = 0;
				$pad_bottom = 0;
				$pad_left   = 0;
				$pad_right  = 0;
				break;
			# make pixel square
			# reduce video size if bigger than p[width] X p[height]
			# and preserve aspect ratio
			case 'max':
				$width        = (float)$i['video_width'   ];
				$height       = (float)$i['video_height'  ];
				$ratio        = (float)$i['video_wh_ratio'];
				$max_width    = (float)$p['video_width'   ];
				$max_height   = (float)$p['video_height'  ];

				# make pixels square
				if( $ratio > 1.0 )
					$width = $height * $ratio;
				else
					$height = @$width / $ratio;

				# reduce width
				if( $width > $max_width ) {
					$r       = $max_width / $width;
					$width  *= $r;
					$height *= $r;
				}

				# reduce height
				if( $height > $max_height ) {
					$r       = $max_height / $height;
					$width  *= $r;
					$height *= $r;
				}

				# make size even (required by many codecs)
				$width  = (integer)( ($width  + 1 ) / 2 ) * 2;
				$height = (integer)( ($height + 1 ) / 2 ) * 2;
				# no padding
				$pad_top    = 0;
				$pad_bottom = 0;
				$pad_left   = 0;
				$pad_right  = 0;
				break;
			# make pixel square
			# resize video to fit inside p[width] X p[height]
			# add padding and preserve aspect ratio
			case 'fit':
				# values need to be multiples of 2 in the end so
				# divide width and height by 2 to do the calculation
				# then multiply by 2 in the end
				$ratio        = (float)$i['video_wh_ratio'];
				$width        = (float)$i['video_width'   ] / 2;
				$height       = (float)$i['video_height'  ] / 2;
				$trt_width    = (float)$p['video_width'   ] / 2;
				$trt_height   = (float)$p['video_height'  ] / 2;

				# make pixels square
				if( $ratio > 1.0 )
					$width = $height * $ratio;
				else
					$height = $width / $ratio;
				
				# calculate size to fit
				$ratio_w = $trt_width  / $width;
				$ratio_h = $trt_height / $height;

				if( $ratio_h > $ratio_w ) {
					$width  = (integer)$trt_width;
					$height = (integer)($width / $ratio);
				} else {
					$height = (integer)$trt_height;
					$width  = (integer)($height * $ratio);
				}

				# calculate padding
				$pad_top    = (integer)(($trt_height - $height + 1) / 2);
				$pad_left   = (integer)(($trt_width  - $width  + 1) / 2);
				$pad_bottom = (integer)($trt_height  - $height - $pad_top );
				$pad_right  = (integer)($trt_width   - $width  - $pad_left);

				# multiply by 2 to undo division and get multiples of 2
				$width      *= 2;
				$height     *= 2;
				$pad_top    *= 2;
				$pad_left   *= 2;
				$pad_bottom *= 2;
				$pad_right  *= 2;
				break;
		}
	}

	
	/** 
	 * Function used to perform all actions when converting a video
	 */
	function ClipBucket()
	{
		$this->start_time_check();
		$this->start_log();
		$this->prepare();

		$ratio = substr($this->input_details['video_wh_ratio'],0,3);
		
		$max_duration = config('max_video_duration') * 60;

		if($this->input_details['duration']>$max_duration)
		{
			$this->log .= "Video duration was ".$this->input_details['duration']." and Max video duration is $max_duration
					<br>Therefore Video cancelled";
			$this->log("conversion_status","failed");
			$this->log("failed_reason","max_duration");
			$this->write_logs();
					
			$this->failed_reason = 'max_duration';
			return false;
		}
				
		if($ratio=='1.7' || $ratio=='1.6')
		{
			$res = $this->configs['res169'];
		} else {
			$res = $this->configs['res43'];
		}
				
		$nr = $this->configs['normal_res'];
		$hr = $this->configs['high_res'];
		$this->configs['video_width'] = $res[$nr][0];
		$this->configs['video_height'] = $res[$nr][1];
		$this->configs['hq_video_width'] = $res[$hr][0];
		$this->configs['hq_video_height'] = $res[$hr][1];

		$this->convert();
		$this->end_time_check();
		$this->total_time();
				
		$this->output_details = $this->get_file_info($this->output_file);
		$this->log .= "\r\n\r\n";
		$this->log_ouput_file_info();
		$this->log .= "\r\n\r\nTime Took : ";
		$this->log .= $this->total_time.' seconds'."\r\n\r\n";
		
		//$this->update_data();
				
		$th_dim = $this->thumb_dim;
		$big_th_dim = $this->big_thumb_dim ;
				
		if(!file_exists($this->output_file))
			$this->log("conversion_status","failed");
		else
			$this->log("conversion_status","completed");

		//Generating Thumb
		if($this->gen_thumbs)
			$this->generate_thumbs($this->output_file,$this->input_details['duration'],$th_dim,$this->num_of_thumbs);
		if($this->gen_thumbs)
			$this->generate_thumbs($this->output_file,$this->input_details['duration'],null,$this->num_of_thumbs,NULL,'med');
		if($this->gen_big_thumb)
			$this->generate_thumbs($this->output_file,$this->input_details['duration'],$big_th_dim,$this->num_of_thumbs,NULL,'big');

		$this->write_logs();
	}
	
	
	
	/**
	 * Function used to generate video thumbnails
	 */
	function generate_thumbs($input_file,$duration,$dim=null,$num=3,$rand=NULL,$sizestring='')
	{
		$tmpDir = TEMP_DIR.'/'.getName($input_file,true);
		mkdir($tmpDir,0777);

		$output_dir = THUMBS_DIR;
		$dimension = '';
		
		if (!$dim) {
			if ($sizestring==='med') {
				$dim='320x180';
			} else {
				$dim='220x124';
			}
		}
		
		if($sizestring!='')
		{
			$sizestring = $sizestring.'-';
		}
		
		if($dim!='original')
		{
			list($width,$height)=explode('x',$dim);
			// expand trick used to add black border when required by changing video size (and aspect ratio) without affecting the effective content aspect ratio 
			// $mplayer_dim = " -vf framestep=I,expand=:::::16/9,scale=$width:$height ";
			
			// fit in a specific square without changing the aspect ratio
			$filtercomplex=" -filter_complex \"select=eq(pict_type\,I),scale=iw*min(".$width."/iw\,".$height."/ih):-1\" ";
			//$filtercomplex=" -filter_complex \"scale=iw*min($width/iw\,$height/ih):-1\" ";
		} else {
			$filtercomplex=" -filter_complex \"select=eq(pict_type\,I)\" ";
		}
		if ($duration<24 && $num>$duration/4) {
			$num=round(duration/4);
			if ($num==0)
				$num=1;
		}
		
		if($num > 1)
		{
			$duration = $duration - 5;
			$division = $duration / $num;
			$count=1;
			
			
			for($id=3;$id<=$duration;$id++)
			{
				$count_string=str_pad($count,2,"0",STR_PAD_LEFT);
				$file_name = getName($input_file, true)."-{$sizestring}{$count_string}.jpg";
				$file_path = THUMBS_DIR.'/'.$file_name;
				
				$id	= $id + $division - 1;
				if($rand != "") {
					$time = $this->ChangeTime($id,1);
				} elseif($rand == "") {
					$time = $this->ChangeTime($id);
				}
				$command = $this->ffmpeg." -y -ss $time -i $input_file -an -vframes 1 -sws_flags lanczos -pix_fmt yuvj420p $filtercomplex $file_path >>/tmp/thumbnail.log 2>&1";
				
				$this->exec($command);
				$count = $count+1;
			}
		} else {
			$file_name = getName($input_file,true)."-{$sizestring}%d.jpg";
			$file_path = THUMBS_DIR.'/'.$file_name;
			$command = $this->ffmpeg." -y -ss 1 -i $input_file -an -vframes 1 -sws_flags lanczos -pix_fmt yuvj420p $filtercomplex $file_path ";
			$this->exec($command);
			$count = $count+1;
		}
		
		rmdir($tmpDir);
	}
	
	
	
	/**
	 * Function used to convert seconds into proper time format
	 * @param : INT duration
	 * @parma : rand
	 */
	 
	function ChangeTime($duration, $rand = "") {
		if($rand != "") {
			$time = gmdate("H:i:s", $duration - rand(0,$duration));
			return $time;
		} elseif($rand == "") {
			$time = gmdate("H:i:s",$duration);
			return $time;
		}
	}
	
	// fit for a resolution by preserving aspect ratio
	// round up to 2
	// 1920x1080 fit to 1280x720
	function rectanglefit($width, $height, $maxwidth, $maxheight) {
		if ($width<=$maxwidth && $height<=$maxheight) {
			return (ceil($width/2.0)*2)."x".(ceil($height/2.0)*2);
		}
		if ($width/$maxwidth>$height/$maxheight) {
			return $maxwidth."x".(ceil($height*$maxwidth/$width/2.0)*2);
		}
		return (ceil($width*$maxheight/$height/2.0)*2)."x".$maxheight;
	}
	
	
	/**
	 * Function used to convert video in HD format
	 * extra : 2nd pass with additionnal encoding formats (HD, webm...)
	 * 
	 * @ Author : Arslan Hassan
	 * @ Software : ClipBucket
	 * @license : AAL
	 */
	
	function convert($extra=false,$input=NULL,$output=NULL,$p=NULL,$i=NULL)
	{
		global $db;
		if(!$input)
			$input = $this->input_file;
		if(!$p)
			$p = $this->configs;
		if(!$i)
			$i = $this->input_details;
		
		$crfboost=0;
		$speed=1;
		if ($i['duration']>2405.0) {
			// longer than 40 min = degrade quality more
			$crfboost=3;
			$speed=2;
		} elseif ($i['duration']>605.0) {
			// longer than 10 min = degrade quality a little
			$crfboost=2;
			$speed=2;
		}
		
		$output_vp9hd=str_replace("-hd.mp4", "-hd.webm", $this->hq_output_file);
		$output_vp9sd=str_replace("-sd.mp4", "-sd.webm", $this->output_file);	
		
		$opt_av = $opt_av_sd = $opt_av_hd = $opt_av_mp4 = $opt_av_mp4sd = $opt_av_mp4hd = $opt_av_vp9  = $opt_av_vp9hd = $opt_av_vp9sd = '';
		
		// should add : " -movflags frag_keyframe " but not working well for streaming
		$opt_av=" -sws_flags lanczos ";
		$opt_av_mp4=" -c:a libfdk_aac -afterburner 1 -c:v libx264 -preset veryslow -threads 0 -movflags +faststart -pix_fmt yuv420p -f mp4 ";
		$opt_av_mp4sd=" -vbr 2 -crf ". (27+$crfboost) ." -vprofile high -level 4 ";
		$opt_av_mp4hd=" -vbr 3 -crf ". (22+$crfboost) ." -vprofile high -level 4.2 ";
		$opt_av_vp9=" -c:v libvpx-vp9  -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 -speed $speed -c:a libopus -vbr on -threads 8 -pix_fmt yuv420p -f webm ";
		$opt_av_vp9sd=" -crf ". (35+$crfboost) ." -b:v 1M -b:a 42k ";
		$opt_av_vp9hd=" -crf ". (27+$crfboost) ." -b:v 3M -b:a 48k ";

		
		//$dims=[[1920,1080],[1280,720],[854,480]];
		
		$iwidth  = $i['video_width'];
		$iheight  = $i['video_height'];
		
		// SD is 480p or smaller, HD is 720p for webm or 1080p for vp9
		$opt_av_sd.=" -s ".$this->rectanglefit($iwidth,$iheight,854,480)." ";
		$opt_av_mp4hd.=" -s ".$this->rectanglefit($iwidth,$iheight,1280,720)." ";
		$opt_av_vp9hd.=" -s ".$this->rectanglefit($iwidth,$iheight,1920,1080)." ";
		
		if (isset($i['video_wh_ratio'])) {
			$opt_av.=" -aspect ".$i['video_wh_ratio']." ";
		}
				
		$output_file = $this->hq_output_file;
		
		if(isset($i['video_rate']) && $i['video_rate']!=0) {
			$fullrate=((float) $i['video_rate']);
			$rrate=$fullrate;
		} else {
			$fullrate=30.0;
		}
		
		// keep default framerate, or halve it if > 30
		// (often 50~60 for some raw source)
		// only VP9 HD is allowed more than 30fps
		if($fullrate > 31.0) {
			$rrate= ((float) $fullrate)/2;
			$rrate=number_format((float)$rrate, 6, '.', '');
			$opt_av_mp4 .= " -r $rrate ";
			$opt_av_vp9sd .= " -r $rrate ";
		}
			
		// set the max keyframe interval to 4 seconds (4 times the output framerate)
		// using same keyframe interval for SD / HD to allow for more consistent seeking
		if (isset($rrate) && $rrate!=0) {
			$grate=$rrate;
		} elseif(isset($p['video_rate']) && $p['video_rate']!=0) {
			$grate=$p['video_rate'];
		} elseif(isset($p['video_max_rate']) && $p['video_max_rate']!=0) {
			$grate=$p['video_max_rate'];
		} else {
			$grate=25.0;
		}
		$opt_av_mp4 .= " -g ". number_format((float)($grate*4.0), 6, '.', '') . " ";
		$opt_av_vp9hd .= " -g ". number_format((float)($fullrate*4.0), 6, '.', '') . " ";
		$opt_av_vp9sd .= " -g ". number_format((float)($grate*4.0), 6, '.', '') . " ";
		
		// audio gain and compression
		// some usefull posts about the filter : http://dsp.stackexchange.com/questions/22442/ffmpeg-audio-filter-pipeline-for-speech-enhancement
		// compand = remove noise (very low background volume), and apply global gain (volgain)
		// dynaudnorm = normalize the volume and make the speech easyer to understand, and less sensitive to microphone position
		if($i['volgain']>3 || $i['volcompress']>9) {
			$volgain=$i['volgain'];
			$volcompress=$i['volcompress'];
			$opt_av .= " -af 'compand=attacks=.1:decays=.4:points=-90/-99 -70/-95 -60/-60 0/0:soft-knee=0.1:volume=-70:gain=$volgain:delay=0.05,dynaudnorm=g=11:s=6.0:p=0.98:m=20.0' ";
		}
		
		if (!$extra) {
			$type = 'SD mp4';
			$command = $this->ffmpeg." -i ".$this->input_file." $opt_av $opt_av_sd $opt_av_mp4 $opt_av_mp4sd ".$this->output_file." >>".$this->log_file." 2>&1";
			
			$this->log .= "\r\n\r\n\n=========STARTING $type CONVERSION==============\r\n\r\n\n";
			$this->log .= date(DATE_ATOM)."\r\n";
			$this->log("$type Video -- Conversion Command",$command);
			$this->log .="\r\n\r\nConversion Details\r\n\r\n";
			$this->exec($command);
			$this->log .= date(DATE_ATOM)."\r\n";
			$this->log .= "\r\n\r\n\n=========ENDING $type CONVERSION==============\n\n";
		} else {		
			$type = 'SD vp9';
			$command = $this->ffmpeg." -i ".$this->input_file." $opt_av $opt_av_sd $opt_av_vp9 $opt_av_vp9sd ".$output_vp9sd." >>".$this->log_file." 2>&1";
		
			$this->log .= "\r\n\r\n\n=========STARTING $type CONVERSION==============\r\n\r\n\n";
			$this->log .= date(DATE_ATOM)."\r\n";
			$this->log("$type Video -- Conversion Command",$command);
			$this->log .="\r\n\r\nConversion Details\r\n\r\n";
			$this->exec($command);
			$this->log .= date(DATE_ATOM)."\r\n";
			$this->log .= "\r\n\r\n\n=========ENDING $type CONVERSION==============\n\n";
			if ($iheight>550) {
				$type = 'HD mp4';
				$command = $this->ffmpeg." -i ".$this->input_file." $opt_av $opt_av_hd $opt_av_mp4 $opt_av_mp4hd ".$this->hq_output_file." >>".$this->log_file." 2>&1";
			
				$this->log .= "\r\n\r\n\n=========STARTING $type CONVERSION==============\r\n\r\n\n";
				$this->log .= date(DATE_ATOM)."\r\n";
				$this->log("$type Video -- Conversion Command",$command);
				$this->log .="\r\n\r\nConversion Details\r\n\r\n";
				$this->exec($command);
				$this->log .= date(DATE_ATOM)."\r\n";
				$this->log .= "\r\n\r\n\n=========ENDING $type CONVERSION==============\n\n";
				
				
				$type = 'HD vp9';
				$command = $this->ffmpeg." -i ".$this->input_file." $opt_av $opt_av_hd $opt_av_vp9 $opt_av_vp9hd ".$output_vp9hd." >>".$this->log_file." 2>&1";

				$this->log .= "\r\n\r\n\n=========STARTING $type CONVERSION==============\r\n\r\n\n";
				$this->log .= date(DATE_ATOM)."\r\n";
				$this->log("$type Video -- Conversion Command",$command);
				$this->log .="\r\n\r\nConversion Details\r\n\r\n";
				$this->exec($command);
				$this->log .= date(DATE_ATOM)."\r\n";
				$this->log .= "\r\n\r\n\n=========ENDING $type CONVERSION==============\n\n";
			}
			
			// log is closed only once : after all encodings have ended
			$fields = array('file_conversion_log',strtolower($type));
			$values = array(mysql_clean($this->log),'yes');
			//$db->update($this->tbl,$fields,$values," id = '".$this->row_id."'");
			$this->write_logs();
		}
		
		return true;
	}

	/**
	 * Function used to create log for a file
	 */
	function write_logs()
	{
		$file = $this->log_file;
		$data = $this->log;
		if (!$data || empty($data))
			return;
		$fo = fopen($file,"a+");
		if(!$fo)
			return;
		fwrite($fo,$data);
		if (!fclose($fo))
			echo "erreur de fermeture du fichier log $file";
		$this->log="";
	}
	
	
	/**
	 * validating video channels
	 */
	function validChannels($in)
	{
		if(!$in)
			return true;
		$in['audio_channels'] = strtolower($in['audio_channels']);
		$channels = false;
		if(is_numeric($in['audio_channels']))
			$channels = $in['audio_channels'];
		else
		{
			if(strstr($in['audio_channels'],'stereo'))
				$channels = 2;
			
			if(strstr($in['audio_channels'],'mono'))
				$channels = 1;
	
			if(!$channels)
			{
				preg_match('/([0-9.]+)/',$in['audio_channels'],$matches);
				if($matches)
					$channels = $matches[1];
			}
		}
		
		if(!$channels)
			return true;
		elseif($channels>2)
			return false;
		else
			return true;
	}
}
?>
