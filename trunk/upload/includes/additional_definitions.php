<?php

# Options de partage de vidéo
$_broadcast_options = array(
    array('id' => 'public',         'lang_id' => 'vdo_br_opt1'),
    array('id' => 'logged',         'lang_id' => 'logged_users_only'),
    array('id' => 'private',        'lang_id' => 'vdo_br_opt2'),
    array('id' => 'unlisted',       'lang_id' => 'vdo_broadcast_unlisted'),
    array('id' => 'unlisted_logged','lang_id' => 'unlisted_logged'),
);
$_bc_opt =& $_broadcast_options;

/**
 * Renvoie un tableau listant les options de partage vidéo, indexé par les identifiants
 * de chaque option, associés à leur description traduite dans la langue par défaut.
 * @return array
 */
function broadcast_options()
{
    global $_bc_opt;
    array_walk(
        $_bc_opt,
        function($opt) use (&$r) { $r[$opt['id']] = lang($opt['lang_id']); },
        $r
    );
    return $r;
}