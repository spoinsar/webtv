<?php

/**
 * License : CBLA
 * Author : Stéphane Poinsart (working at the Université de Technologie de Compiègne)
 */


define("THIS_PAGE","videos");

include("../includes/config.inc.php");

$cat = $_GET['cat'];
$catcode = $_GET['catcode'];
if (!$cat || !$catcode || sha1("".$cat.URL_SEED)!=$catcode) {
	echo "URL problem... please check that this page or frame is called correctly.";
	die();
}

if (! user_id('force_check_CAS')) {
	redirect_to_cas();
	die();
}

$params['category'] = array_keys($cbvid->get_tree_flat(mysql_clean($cat)));
	
//$params['featured']='yes';
$params['limit']='16';
$params['order'] = ' date_added DESC ';
$params['show_hidden'] = true;
	
assign('moodle_vids',get_videos($params));
	
Template('blocks/iframe_moodle_video.html', true);


?>