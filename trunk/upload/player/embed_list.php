<?php

/**
 * License : CBLA
 * Author : Stéphane Poinsart, on behalf of Université de Technologie de Compiègne
 */

/**
 * iFrame based embed player ClipBucket
 * reason to use iFrame instead of embed code
 * is to control player with full support of javascript
 */


define("THIS_PAGE","videos");

include("../includes/config.inc.php");

// the default video category that will be displayed on the www.utc.fr homepage is hardcoded for now
//$cat = (isset($_GET['cat'])) ? $_GET['cat'] : 95;
//$params['category'] = array_keys($cbvid->get_tree_flat(mysql_clean($cat)));

$params['tags'] = 'homepage';

$params['featured']='yes';
$params['limit']='3';
$params['order'] = ' featured_date DESC ';

assign('featured_vids',get_videos($params));

Template('blocks/editor_pick/iframe_featured_video.html', true);
?>