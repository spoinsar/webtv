<?php
/*
	Player Name: jwplayer5
	Description: Updated ClipBucket Player with all required features
	Author: Based on Arslan Hassan, modified by Stephane P
	ClipBucket Version: 2
	Plugin Version: 1.0
	Website: http://clip-bucket.com/cb-player

 * @Author : Arslan Hassan
 * @Script : ClipBucket v2
 * @License : Attribution Assurance License -- http://www.opensource.org/licenses/attribution.php
 * @Since : September 15 2009
 *
 * CBPlayer or ClipBucket player is based on JW Player source, anything that works
 * with JWPlayer will ultimately work with Pakplayer
 * CBPlayer license comes under AAL(OSI) please read our license agreement carefully
 */

$jw_player5 = false;

if (file_exists(BASEDIR."/plugins/amara/player_helper.php"))
	include_once(BASEDIR."/plugins/amara/player_helper.php");

if(!function_exists('jw_player5'))
{
	define("JW_PLAYER5",basename(dirname(__FILE__)));
	define("JW_PLAYER5_DIR",PLAYER_DIR."/".JW_PLAYER5);
	define("JW_PLAYER5_URL",PLAYER_URL."/".JW_PLAYER5);
	assign('jw_player5_dir',JW_PLAYER5_DIR);
	assign('jw_player5_url',JW_PLAYER5_URL);
	
	
	function jw_player5($in)
	{
		global $jw_player5;
		$jw_player5 = true;
		
		$vdetails = $in['vdetails'];
		$vid_file = get_video_file($vdetails,true,true);
		if (function_exists(get_subtitles_list) && $vdetails["subtitles"]!="disable") {
			$subtitles_list=get_subtitles_list($vdetails["file_name"]);
			if ($subtitles_list!=null) {
				assign('subtitles_languages_list',implode(",",array_keys($subtitles_list)));
				assign('subtitles_url_list',implode(",",array_values($subtitles_list)));
			}
		}
		
		//Checking for YT Referal
		if(function_exists('get_refer_url_from_embed_code'))
		{
			$ref_details = get_refer_url_from_embed_code(unhtmlentities(stripslashes($vdetails['embed_code'])));
			$ytcode = $ref_details['ytcode'];
		}
		
		if($vid_file || $ytcode)
		{
			$hd = $data['hq'];
			
			if($hd=='yes') $file = get_hq_video_file($vdetails); else $file = get_video_file($vdetails,true,true);
			$hd_file = get_hq_video_file($vdetails);
			
			
			if($ytcode)
			{
				assign('youtube',true);
				assign('ytcode',$ytcode);
			}
			
			if(!strstr($in['width'],"%"))
				$in['width'] = $in['width'].'px';
			if(!strstr($in['height'],"%"))
				$in['height'] = $in['height'].'px';
		
			if($in['autoplay'] =='yes' || $in['autoplay']===true || 
			($_COOKIE['auto_play_playlist'] && ($_GET['play_list'] || $_GET['playlist'])))
			{
				$in['autoplay'] = true;
			}else{
				$in['autoplay'] = false;
			}
			
			
			//Logo Placement
			assign('logo_placement',jw_player5_logo_position());
			assign('logo_margin',config('logo_padding'));
			
			//Setting Skin
			assign('cb_skin','modieus/modieus.xml');
			
			assign('player_data',$in);
			assign('player_logo',website_logo());
			assign('normal_vid_file',$vid_file);
			assign("hq_vid_file",$hd_file);			
			assign('vdata',$vdetails);
			Template(JW_PLAYER5_DIR.'/jwplayer5.html',false);
			
			return true;
		}
	}
	
	/**
	 * Setting logo position for CB Player
	 */
	function jw_player5_logo_position($pos=false)
	{
		if(!$pos)
			$pos = config('logo_placement');
		switch($pos)
		{
			case "tl":
			$position = "top-left";
			break;
			
			case "tr":
			$position = "top-right";
			break;
			
			case "br":
			$position = "bottom-right";
			break;
			
			case "bl":
			$position = "bottom-left";
			break;
			
		}
		
		return $position;
	}
	
	/**
	 * This function generates src for embedable link
	 * which can be used in OBJECT tag to embed videos on a website
	 *
	 * @param video details
	 * @return src link
	 */
	function jwplayer5_embed_src($vdetails)
	{	
		$config  = urlencode(BASEURL."/player/".JW_PLAYER5."/embed_player.php?vid="
		.$vdetails['videoid']."&autoplay=".config('autoplay_embed'));
		
		$embed_src = BASEURL.'/player/'.JW_PLAYER5.'/player.swf?config='.$config;

		
		if(function_exists('get_refer_url_from_embed_code'))
		{
			$ref_details = get_refer_url_from_embed_code(unhtmlentities(stripslashes($vdetails['embed_code'])));
			$ytcode = $ref_details['ytcode'];
		}
		
		if(!$vdetails['embed_code']  || $vdetails['embed_code'] =='none'|| $ytcode)
		{
			$code = '<embed src="'.$embed_src.'" type="application/x-shockwave-flash"';
			$code .= 'allowscriptaccess="always" allowfullscreen="true"  ';
			$code .= 'width="'.config("embed_player_width").'" height="'.config("embed_player_height").'"></embed>';
			return $code;
		}else
			return false;
	}
	
	/**
	 * Writing CB Player function to play videos on facebook
	 */
	function cb_facebook_embed($params)
	{
		$vdetails = $params['video'];
		$config  = urlencode(BASEURL."/player/".JW_PLAYER5."/embed_player.php?vid="
		.$vdetails['videoid']."&autoplay=".config('autoplay_embed'));
		$embed_src = BASEURL.'/player/'.JW_PLAYER5.'/player.swf?config='.$config;	
		return $embed_src;
	}
		
	
	register_embed_function('jwplayer5_embed_src');
	register_actions_play_video('jw_player5');
	cb_register_function('cb_facebook_embed','fb_embed_video');
	//include Pak Player JS File
	$Cbucket->add_header(JW_PLAYER5_DIR.'/jwplayer5_header.html');
	$Cbucket->add_admin_header(JW_PLAYER5_DIR.'/jwplayer5_header.html');
	
	/**
	 * Including plugin files 
	 */
	include("jwplayer5.plugin.php");
}
?>