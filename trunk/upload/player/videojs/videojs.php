<?php
/*
   Player Name: Video JS 1.0
   Description: HTML5 Player with Many HOT Features.
   Author: Arslan Hassan (original author), Stephane Poinsart on behalf of www.utc.fr (reworked for UTC usage)
   Min Version : 3.0
   Plugin Version: 1.0
   Website: http://clip-bucket.com/pak-player
 * @Author : Arslan Hassan
 * @Script : ClipBucket v3.0
 * @Since : 7/2/2012
 */

if (!defined(VIDEOJS_DIR_NAME)) {
	if (file_exists(BASEDIR."/plugins/amara/player_helper.php"))
		include_once(BASEDIR."/plugins/amara/player_helper.php");
	
    define('VIDEOJS_DIR_NAME',basename(dirname(__FILE__)));
    define("VIDEOJS_DIR",PLAYER_DIR.'/'.VIDEOJS_DIR_NAME);
    define("VIDEOJS_URL",PLAYER_URL.'/'.VIDEOJS_DIR_NAME);
    
    /* CONFIGS */
    assign('videojs_url',VIDEOJS_URL);
    
    /**
     * PLayer video in video-js player
     * @global boolean $pak_player
     * @param type $in
     * @return boolean 
     */
    function videojs($in)
    {
    	$vdetails = $in['vdetails'];
    	$mp4_sd = get_video_file($vdetails,true,true);
    	
    	if (function_exists(get_subtitles_list) && $vdetails["subtitles"]!="disable") {
    		$subtitles_list=get_subtitles_list($vdetails["file_name"]);
    		if ($subtitles_list!=null) {
    			assign('subtitles_list',$subtitles_list);
    		}
    	}
    	
    	//Checking for YT Referal
    	if(function_exists('get_refer_url_from_embed_code'))
    	{
    		$ref_details = get_refer_url_from_embed_code(unhtmlentities(stripslashes($vdetails['embed_code'])));
    		$ytcode = $ref_details['ytcode'];
    	}
    	
    	if($mp4_sd || $ytcode)
    	{
    		$mp4_hd = get_hq_video_file($vdetails);
    		if ($mp4_hd==$mp4_sd) {
    			$mp4_hd=null;
    		}
    		$vp9_sd=get_video_file($vdetails,false,true,false,false,$ext="-sd.webm");
    		$vp9_hd=get_video_file($vdetails,false,true,false,false,$ext="-hd.webm");
    		
    		if($ytcode)
    		{
    			assign('youtube',true);
    			assign('ytcode',$ytcode);
    		}
    		
    		if(!$in['width'] || strstr($in['width'],"%") || $in['width']<80) {
    			$in['width']=320;
    		}
    		$in['width'] = $in['width'].'px';
    		
    		if(!$in['height'] || strstr($in['height'],"%") || $in['height']<45) {
    				$in['height']=180;
    		}
    		$in['height'] = $in['height'].'px';
    		
    		assign('player_data',$in);
    		assign('mp4_sd',$mp4_sd);
    		assign("mp4_hd",$mp4_hd);
    			
    		assign("vp9_sd",$vp9_sd);
    		assign("vp9_hd",$vp9_hd);
    			
    		assign('vdata',$vdetails);
    		Template(VIDEOJS_DIR.'/player.html',false);
    			
    		return true;
    	}
    }
    
    register_actions_play_video('videojs');
    
	$Cbucket->add_header(VIDEOJS_DIR.'/videojs_header.html');
	$Cbucket->add_admin_header(VIDEOJS_DIR.'/videojs_header.html');
}
?>