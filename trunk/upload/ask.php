<?php

/**
 * @author : Mickaël Urrutia
 * @desc : Page d'appel par des applications extérieures pour obtenir des informations
 * sur la WebTV.
 * @return : Ca dépend de la requête, mais devrait généralement renvoyer une chaîne JSON
 */
define("THIS_PAGE", 'ask');

require 'includes/config.inc.php';

/**/
#===============================================================================
# test de sécurité 
if (!isset($_REQUEST['token'])) 
    send_json(false, 'Clef de sécurité manquante');

# reconstruire la clef et la comparer à celle envoyée
$correct_token = md5($_REQUEST['q'] . URL_SEED);
if ($_REQUEST['token'] != $correct_token)
    send_json(false, 'Clef de sécurité inexacte');

# fin du test de sécurité
#===============================================================================
/**/

$q = $_REQUEST['q'];

if (!empty($q)) {
    switch ($q) {
        case 'cat_tree':
            # TODO : l'arbre des categories devrait peut-être être nettoyé pour
            # éviter de mettre à disposition trop d'infos
            $root = (isset($_REQUEST['root'])) ? $_REQUEST['root'] : 0;
            $tree = $cbvid->get_tree($root);
            send_json(true, $tree);
            
        case 'delete_video':
            $video = mysql_clean($_REQUEST['v']);
            if (! $cbvideo->video_exists($video)) {
                send_json(false, "Impossible de supprimer la vidéo [$video], elle n'existe pas (ou plus) sur la WebTV.");
            }
            #### La méthode 'force_delete_video()' est un trou énorme de sécurité !!!
            #### il faut très rapidement changer cette action par une version vérifiant les permissions de l'utilisateur !!!
            $msg = $cbvideo->force_delete_video($video); 
            # si la vidéo n'existe plus, c'est un succès!
            send_json(($cbvideo->video_exists($video) == 0), $msg);

        case 'video_exists':
            $video = mysql_clean($_REQUEST['v']);
            $exists = $cbvideo->video_exists($video);
            send_json($exists, $_REQUEST);
        case 'videos_exist':
            $vids = mysql_clean($_REQUEST['vids']);
            $exists= $cbvideo->videos_exists($vids);
            send_json($exists, $_REQUEST);
            
        case 'cat_frame_url':
            $cat_id = $_REQUEST['cat_id'];
            $url = trim(get_server_url(), '/') .'/player/moodle_list.php?cat='.$cat_id .'&catcode='. sha1(''.$cat_id.URL_SEED);
            send_json(true, array('url'=>$url));
        
        case 'video_infos':
            $vid = mysql_clean($_REQUEST['v']);
            if (! $cbvideo->video_exists($vid)) {
                send_json(false, "La vidéo avec l'identifiant [$vid] n'existe pas ou a été supprimée de la WebTV.");
            }
            $details = $cbvideo->get_video_more_detailed($vid);
            $r = array();
            foreach ($details as $key => $val) if (! is_numeric($key)) $r[$key] = $val;
            send_json(true, $r);

        case 'modify':
            $fields = ['title', 'description', 'category', 'tags', 'broadcast'];
            $vals = [$_POST['title'], $_POST['description'], '#'.$_POST['category'].'#', $_POST['tags'], $_POST['broadcast']];
            $db->update(tbl('video'), $fields, $vals," videokey='{$_POST['v']}'");
            send_json(true, "modifications effectuées");

        # options de partage
        case 'broadcast':
            send_json(true, broadcast_options());

        default :
            send_json(false, "aucune action n'est configurée pour le paramètre [q=$q]", $_REQUEST);
            break;
    }
} else {
    send_json(false, "erreur : le paramètre [q] n'a pas été renseigné", $_REQUEST);
}