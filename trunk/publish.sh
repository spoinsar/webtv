#! /bin/bash


if [ $# -ne 1 ] ; then
	echo "Usage: `basename $0` <username ssh sur dendrite>"
	exit 1
fi

type rsync >/dev/null 2>&1 || {
	echo "rsync program not found, must be installed to continue"
	exit 1
}

type ssh >/dev/null 2>&1 || {
	echo "ssh program not found, must be installed to continue"
	exit 1
}

rsync -rlvz -e ssh --exclude 'files' --exclude 'cache' --exclude 'images/category_thumbs' --exclude includes/dbconnect.php upload/  $1@webtv-interne.utc:/usr/src/clipbucket/upload
